var express = require('express'),
  config = require('./config/config'),
  bodyParser = require('body-parser'),
  compress = require('compression');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(compress());
app.use(express.static(config.root + '/'));

app.get('/', function(req, res) {
    res.sendFile(config.root + '/index.html');
});

app.listen(config.port, function () {
  console.log('Express server listening on port ' + config.port);
});